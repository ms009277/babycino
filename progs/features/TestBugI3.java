class TestBugI3 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        int x;
        x = 0;
        x++; // Correct behavior: x should be incremented to 6.
        if (x == 0) {
            // This condition will never be true because x is incremented before the check.
            // Therefore, the message "Error: x was not incremented." will never be printed.
            System.out.println("Error: x was not incremented.");
        }
        return x;
    }
}
