class TestBugI2 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        int x;
        x = 5;
        x++; 
        if (x == 1) {
            System.out.println();
        } else if ( x == 6){
            System.out.println();
        }
        return x;
    }
}