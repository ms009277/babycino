class TestFeatureI {
    public static void main(String[] a) {
        new Test().f();
    }
}

class Test {
    public void f() {
        boolean boolValue = false; // Start with false.
        int count = 1;

        System.out.println("Initial boolean value: " + boolValue);

        while (count < 11) {
            // Simulate "incrementing" the boolean by toggling its state.
            boolValue = !boolValue;

            // Print the toggled state of the boolean variable.
            System.out.println("Toggled boolean value at count " + count + ": " + boolValue);

            count++;
        }
    }
}
